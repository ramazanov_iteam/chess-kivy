# -*- coding: utf-8 -*-

# Импорт модулей
import position
from enums import GameStatus, GameResult, Sides, Pieces
from utils import is_king, is_queen, is_rook, is_knight, is_bishop, is_pawn
from utils import piece_color, sign

from copy import deepcopy

from kivy.event import EventDispatcher
from  kivy.properties import NumericProperty

# Проверяет нападают ли на данную клетку
def the_cell_is_under_attack(position, y, x, side=None):
    
    # Если сторона не задана, то будем считать что это тот, кто ходит
    if side is None:
        side = position.whose_turn
        
    # Проверяем каждую клетку
    for fy in range(8):
        for fx in range(8):
            
            # Подготавливаем переменные
            dt = (y - fy) * (y - fy) + (x - fx) * (x - fx)
            dy = sign(y - fy); dx = sign(x - fx)
            
            # Если эта клетка пуста или на ней стоит наша фигура, то можно не рассматривать данную клетку
            if (position.piece_placement[fy][fx] is None) or (piece_color(position.piece_placement[fy][fx]) == side):
                continue
                
            # Вражеский король может бить только соседнюю клетку
            if is_king(position.piece_placement[fy][fx]):
                if dt <= 2: return True
                
            # Вражеский ферзь может бить по прямой или по диагонали
            elif is_queen(position.piece_placement[fy][fx]):
                if (y == fy) or (x == fx) or (abs(y - fy) == abs(x - fx)):
                    ny = fy + dy; nx = fx + dx
                    while (cmp((ny, nx), (y, x)) != 0) and (position.piece_placement[ny][nx] is None):
                        ny += dy; nx += dx
                    if cmp((ny, nx), (y, x)) == 0: return True

            # Вражеская ладья может бить только по прямой
            elif is_rook(position.piece_placement[fy][fx]):
                if (y == fy) or (x == fx):
                    ny = fy + dy; nx = fx + dx
                    while (cmp((ny, nx), (y, x)) != 0) and (position.piece_placement[ny][nx] is None):
                        ny += dy; nx += dx
                    if cmp((ny, nx), (y, x)) == 0: return True

            # Вражеский конь может бить только буквой "Г"
            elif is_knight(position.piece_placement[fy][fx]):
                if dt == 5: return True

            # Вражеский слон может бить только по диагонали
            elif is_bishop(position.piece_placement[fy][fx]):
                if abs(y - fy) == abs(x - fx):
                    ny = fy + dy; nx = fx + dx
                    while (cmp((ny, nx), (y, x)) != 0) and (position.piece_placement[ny][nx] is None):
                        ny += dy; nx += dx
                    if cmp((ny, nx), (y, x)) == 0: return True

            # Вражеская пешка может бить вперед и влево или вперед и вправо на соседнюю клетку
            # Вражеская пешка может бить нашу пешку "на проходе"
            elif is_pawn(position.piece_placement[fy][fx]):
                dy = -1 if side == Sides.WHITE else 1
                if (y == fy + dy) and (abs(x - fx) == 1): return True
                if (position.en_passant_target_square is not None) and (y == fy) and (abs(x - fx) == 1):
                    if (y == position.en_passant_target_square[0] - dy) and (x == position.en_passant_target_square[1]):
                        return True
                    
    # Если ни на одну клетку не нападают возвращаем True
    return False


# Проверяет есть ли король и нападают ли на него
def the_king_is_in_check(position, side):
    
    # Сначала определяем, где король
    kings_coord = None
    for y in range(8):
        for x in range(8):
            if is_king(position.piece_placement[y][x]) and (piece_color(position.piece_placement[y][x]) == side):
                kings_coord = (y, x)
                
    # Если короля нет, возвращаем True
    if kings_coord is None:
        return True
    
    # Вот теперь проверяем нападают на короля или нет
    return the_cell_is_under_attack(position, *kings_coord, side=side)


# Если король находится под шахом, возвращает его координаты, иначе None
def get_in_check_kings_cell(position):

    # Сначала определяем, где король
    kings_coord = None
    for y in range(8):
        for x in range(8):
            if is_king(position.piece_placement[y][x]) and (piece_color(position.piece_placement[y][x]) == position.whose_turn):
                kings_coord = (y, x)
                
    # Если короля нет, возвращаем None
    if kings_coord is None:
        return None
        
    # Если нападают на короля, то возвращаем его координаты, иначе None
    if the_cell_is_under_attack(position, *kings_coord):
        return kings_coord
    else:
        return None


# Определяет возможные ходы
def get_possible_moves(game):
    
    # Вычисляем возможные ходы, если игра не окончена
    result = []
    for x in range(8):
        for y in range(8):
            
            # Запоминаем фигуру, которая ходит
            p = game.position.piece_placement[y][x]
            
            # С пустой клетки нельзя ходить.
            # Также нельзя ходить фигурой противника
            if (p is None) or (piece_color(p) != game.position.whose_turn):
                continue
            
            # Можно ли белым сделать короткую рокировку
            if (p == Pieces.WHITE_KING) and (cmp((y, x), (0, 4)) == 0):
                if game.position.white_can_kings_side_castling and (game.position.piece_placement[0][7] == Pieces.WHITE_ROOK):
                    if (game.position.piece_placement[0][5] is None) and (game.position.piece_placement[0][6] is None):
                        if (not the_cell_is_under_attack(game.position, 0, 4)) and (not the_cell_is_under_attack(game.position, 0, 5)):
                            result.append((4, 0, 6, 0, None))
            
            # Можно ли белым сделать длинную рокировку
            if (p == Pieces.WHITE_KING) and (cmp((y, x), (0, 4)) == 0):
                if game.position.white_can_queens_side_castling and (game.position.piece_placement[0][0] == Pieces.WHITE_ROOK):
                    if (game.position.piece_placement[0][3] is None) and (game.position.piece_placement[0][2] is None) and (game.position.piece_placement[0][1] is None):
                        if (not the_cell_is_under_attack(game.position, 0, 4)) and (not the_cell_is_under_attack(game.position, 0, 3)):
                            result.append((4, 0, 2, 0, None))
            
            # Можно ли черным сделать короткую рокировку
            if (p == Pieces.BLACK_KING) and (cmp((y, x), (7, 4)) == 0):
                if game.position.black_can_kings_side_castling and (game.position.piece_placement[7][7] == Pieces.BLACK_ROOK):
                    if (game.position.piece_placement[7][5] is None) and (game.position.piece_placement[7][6] is None):
                        if (not the_cell_is_under_attack(game.position, 7, 4)) and (not the_cell_is_under_attack(game.position, 7, 5)):
                            result.append((4, 7, 6, 7, None))
            
            # Можно ли черным сделать длинную рокировку
            if (p == Pieces.BLACK_KING) and (cmp((y, x), (7, 4)) == 0):
                if game.position.black_can_queens_side_castling and (game.position.piece_placement[7][0] == Pieces.BLACK_ROOK):
                    if (game.position.piece_placement[7][3] is None) and (game.position.piece_placement[7][2] is None) and (game.position.piece_placement[7][1] is None):
                        if (not the_cell_is_under_attack(game.position, 7, 4)) and (not the_cell_is_under_attack(game.position, 7, 3)):
                            result.append((4, 7, 2, 7, None))
            
            # Король также может ходить на соседние клетки
            if is_king(p):
                possible_target_squares = [(y + 1, x - 1), (y + 1, x), (y + 1, x + 1), (y, x + 1), (y - 1, x + 1),
                                           (y - 1, x), (y - 1, x - 1), (y, x - 1)]
                for (ny, nx) in possible_target_squares:
                    if (ny in range(8)) and (nx in range(8)):
                        if (game.position.piece_placement[ny][nx] is None) or (
                            piece_color(game.position.piece_placement[ny][nx]) != game.position.whose_turn):
                            result.append((x, y, nx, ny, None))
            
            # Ладья и ферзь могут делать ходы по прямой на любое количество клеток
            # Слон и ферзь могут делать ходы по диагонали на любое количество клеток
            ds = []
            if is_rook(p) or is_queen(p):
                ds.extend([(1, 0), (0, 1), (-1, 0), (0, -1)])
            if is_bishop(p) or is_queen(p):
                ds.extend([(-1, -1), (1, 1), (1, -1), (-1, 1)])
            for (dy, dx) in ds:
                ny = y + dy; nx = x + dx
                while (ny in range(8)) and (nx in range(8)) and (game.position.piece_placement[ny][nx] is None):
                    result.append((x, y, nx, ny, None))
                    ny += dy; nx += dx
                if (ny in range(8)) and (nx in range(8)) and (
                    piece_color(game.position.piece_placement[ny][nx]) != game.position.whose_turn):
                    result.append((x, y, nx, ny, None))
            
            # Конь может ходить буквой "Г"
            if is_knight(p):
                possible_target_squares = [(y + 2, x - 1), (y + 2, x + 1), (y + 1, x + 2), (y - 1, x + 2),
                                           (y - 2, x + 1), (y - 2, x - 1), (y - 1, x - 2), (y + 1, x - 2)]
                for (ny, nx) in possible_target_squares:
                    if (ny in range(8)) and (nx in range(8)):
                        if (game.position.piece_placement[ny][nx] is None) or (
                            piece_color(game.position.piece_placement[ny][nx]) != game.position.whose_turn):
                            result.append((x, y, nx, ny, None))
            
            # Пешка ходит на одну клетку вперед (тихий ход)
            # На две клетки вперед с исходной позиции (тихий ход)
            # Бьет вперед и влево или вперед и вправо на соседнюю клетку (ударный ход)
            # Бьет пешку противника "на проходе"
            # На последней горизонтали превращается в фигуру
            if is_pawn(p):
                dy, my, fy = (1, 7, 1) if game.position.whose_turn == Sides.WHITE else (-1, 0, 6)
                possible_target_squares = []
                if game.position.piece_placement[y + dy][x] is None:
                    possible_target_squares.append((y + dy, x))
                if (y == fy) and (game.position.piece_placement[y + dy][x] is None) and (game.position.piece_placement[y + dy + dy][x] is None):
                    result.append((x, y, x, y + dy + dy, None))
                if (x > 0) and (game.position.piece_placement[y + dy][x - 1] is not None) and (piece_color(game.position.piece_placement[y + dy][x - 1]) != game.position.whose_turn):
                    possible_target_squares.append((y + dy, x - 1))
                if (x < 7) and (game.position.piece_placement[y + dy][x + 1] is not None) and (piece_color(game.position.piece_placement[y + dy][x + 1]) != game.position.whose_turn):
                    possible_target_squares.append((y + dy, x + 1))
                if game.position.en_passant_target_square is not None:
                    if (x > 0) and (cmp((y + dy, x - 1), game.position.en_passant_target_square) == 0):
                        result.append((x, y, x - 1, y + dy, None))
                    if (x < 7) and (cmp((y + dy, x + 1), game.position.en_passant_target_square) == 0):
                        result.append((x, y, x + 1, y + dy, None))
                for (ny, nx) in possible_target_squares:
                    if ny == my:
                        if game.position.whose_turn == Sides.WHITE:
                            result.append((x, y, nx, ny, Pieces.WHITE_QUEEN))
                            result.append((x, y, nx, ny, Pieces.WHITE_ROOK))
                            result.append((x, y, nx, ny, Pieces.WHITE_KNIGHT))
                            result.append((x, y, nx, ny, Pieces.WHITE_BISHOP))
                        else:
                            result.append((x, y, nx, ny, Pieces.BLACK_QUEEN))
                            result.append((x, y, nx, ny, Pieces.BLACK_ROOK))
                            result.append((x, y, nx, ny, Pieces.BLACK_KNIGHT))
                            result.append((x, y, nx, ny, Pieces.BLACK_BISHOP))
                    else:
                        result.append((x, y, nx, ny, None))
    
    # Короля нельзя есть
    # После хода король не должен находиться под боем
    copyresult = result
    result = []
    for move in copyresult:
        if is_king(game.position.piece_placement[move[3]][move[2]]):
            continue
        next_position = get_position_after_move(game.position, *move)
        if the_king_is_in_check(next_position, game.position.whose_turn):
            continue
        result.append(move)
        
    # Возвращаем результат
    return result


# Определяет какая позиция получится после хода
def get_position_after_move(position, x1, y1, x2, y2, np):
    
    # Делаем резервную копию позиции
    position = deepcopy(position)
    
    # Запоминаем фигуру, которая ходит
    p = position.piece_placement[y1][x1]
    
    # Обновляем счетчик полуходов
    if (position.piece_placement[y2][x2] is not None) or is_pawn(p):
        position.halfmove_clock = 0
    else:
        position.halfmove_clock += 1
    
    # Перемещаем фигуру
    position.piece_placement[y2][x2] = p
    position.piece_placement[y1][x1] = None
    
    # Делаем рокировку
    if is_king(p):
        if abs(x2 - x1) == 2:
            if x2 > x1:
                position.piece_placement[y2][x2 - 1] = position.piece_placement[y2][x2 + 1]
                position.piece_placement[y2][x2 + 1] = None
            else:
                position.piece_placement[y2][x2 + 1] = position.piece_placement[y2][x2 - 2]
                position.piece_placement[y2][x2 - 2] = None
    
    # Бьем пешку на проходе
    if is_pawn(p):
        if (x2 != x1) and (position.en_passant_target_square is not None):
            if cmp((y2, x2), position.en_passant_target_square) == 0:
                position.piece_placement[y1][x2] = None
    
    # Превращаем пешку в фигуру
    if is_pawn(p):
        if y2 % 7 == 0:
            position.piece_placement[y2][x2] = np
    
    # Если ход черных, увеличиваем номер хода на единицу
    if position.whose_turn == Sides.BLACK:
        position.fullmove_number += 1
    
    # Меняем очередь хода
    if position.whose_turn == Sides.WHITE:
        position.whose_turn = Sides.BLACK
    else:
        position.whose_turn = Sides.WHITE
    
    # Обновляем информацию о возможности рокировки
    if (p == Pieces.WHITE_KING) or (cmp((y1, x1), (0, 7)) == 0) or (cmp((y2, x2), (0, 7)) == 0):
        position.white_can_kings_side_castling = False
    if (p == Pieces.WHITE_KING) or (cmp((y1, x1), (0, 0)) == 0) or (cmp((y2, x2), (0, 0)) == 0):
        position.white_can_queens_side_castling = False
    if (p == Pieces.BLACK_KING) or (cmp((y1, x1), (7, 7)) == 0) or (cmp((y2, x2), (7, 7)) == 0):
        position.black_can_kings_side_castling = False
    if (p == Pieces.BLACK_KING) or (cmp((y1, x1), (7, 0)) == 0) or (cmp((y2, x2), (7, 0)) == 0):
        position.black_can_queens_side_castling = False
    
    # Определяем проходную клетку
    if is_pawn(p) and (abs(y2 - y1) == 2):
        if p == Pieces.WHITE_PAWN:
            position.en_passant_target_square = (y1 + 1, x2)
        else:
            position.en_passant_target_square = (y1 - 1, x2)
    else:
        position.en_passant_target_square = None
    
    # Возвращаем результат
    return position


# Возвращает статус и результат игры
def get_status_and_result(game, force_recheck_status=False):
    
    # Соответствие результатов статусам игры
    set_results = {GameStatus.THE_GAME_IS_ONGOING:         GameResult.THE_GAME_IS_ONGOING,
                   GameStatus.WHITE_HAS_BEEN_CHECKMATED:   GameResult.BLACK_WON,
                   GameStatus.BLACK_HAS_BEEN_CHECKMATED:   GameResult.WHITE_WON,
                   GameStatus.STALEMATE:                   GameResult.DRAW,
	               GameStatus.WHITE_RESIGNS:               GameResult.BLACK_WON,
                   GameStatus.BLACK_RESIGNS:               GameResult.WHITE_WON,
                   GameStatus.WHITE_WON_ON_TIME:           GameResult.WHITE_WON,
                   GameStatus.BLACK_WON_ON_TIME:           GameResult.BLACK_WON,
                   GameStatus.WHITE_WON_BY_FORFEIT:        GameResult.WHITE_WON,
                   GameStatus.BLACK_WON_BY_FORFEIT:        GameResult.BLACK_WON,
	               GameStatus.THREEFOLD_REPETITION:        GameResult.DRAW,
                   GameStatus.FIFTY_MOVE_RULE:             GameResult.DRAW,
                   GameStatus.FIVEFOLD_REPETITION:         GameResult.DRAW,
                   GameStatus.SEVENTY_FIVE_MOVE_RULE:      GameResult.DRAW,
                   GameStatus.INSUFFICIENT_MATERIAL:       GameResult.DRAW,
                   GameStatus.DRAW_ON_TIME:                GameResult.DRAW,
                   GameStatus.WHITE_OFFER_A_DRAW:          GameResult.THE_GAME_IS_ONGOING,
                   GameStatus.BLACK_OFFER_A_DRAW:          GameResult.THE_GAME_IS_ONGOING,
                   GameStatus.WHITE_ACCEPT_THE_DRAW:       GameResult.DRAW,
                   GameStatus.BLACK_ACCEPT_THE_DRAW:       GameResult.DRAW}
    
    # Если надо перепроверять, временно ставим статус игры в "Идет игра"
    status = GameStatus.THE_GAME_IS_ONGOING if force_recheck_status else game.status
    result = set_results[status]
    
    # Если игра не закончена
    if result == GameResult.THE_GAME_IS_ONGOING:
        
        # Если нет возможного хода для игрока, то это матовая ситуация или пат
        if not game.possible_moves:
        
            # Если на короля не нападают, то это пат
            if game.in_check_kings_cell is None:
                status = GameStatus.STALEMATE
                
            # Если ход белых, и его король под шахом, то это мат белым
            elif game.position.whose_turn == Sides.WHITE:
                status = GameStatus.WHITE_HAS_BEEN_CHECKMATED
            
            # Если ход черных, и его король под шахом, то это мат черным
            else:
                status = GameStatus.BLACK_HAS_BEEN_CHECKMATED
    
    # Возвращаем результат
    return status, set_results[status]


# Класс игры
class Game(EventDispatcher):

    # Статус игры
    status = NumericProperty(GameStatus.THE_GAME_IS_ONGOING)
    
    # Инициализация класса игры
    def __init__(self):
    
        # Определяем какие события у нас есть и вызываем конструктор базового класса
        self.register_event_type('on_start_new')
        self.register_event_type('on_move')
        super(self.__class__, self).__init__()
        
    # Начинает новую игру
    def start_new(self):
    
        # Начинаем игру
        self.position = position.initial()
        self.positions_frequence = {self.position.notation(): 1}
        self.positions_history = [self.position]
        self.moves_history = []
        self.possible_moves = get_possible_moves(self)
        self.in_check_kings_cell = get_in_check_kings_cell(self.position)
        self.status, self.result = get_status_and_result(self, force_recheck_status=True)
        
        # Запускаем событие on_start_new (началась новая игра)
        self.dispatch('on_start_new')
        
    # Обработчик по умолчанию для события on_start_new (началась новая игра) ничего не делает
    def on_start_new(self, *args):
        pass
    
    # Обработчик по умолчанию для события on_move (ход сделан) ничего не делает
    def on_move(self, *args):
        pass
    
    # Совершает ход
    def do_move(self, x1, y1, x2, y2, np=None):
        
        # Если игра закончена, возвращаем False
        if self.result != GameResult.THE_GAME_IS_ONGOING:
            return False
        
        # Если ход невозможный, то отменяем все и возвращаем False
        if not ((x1, y1, x2, y2, np) in self.possible_moves):
            return False
        
        # Делаем ход
        self.position = get_position_after_move(self.position, x1, y1, x2, y2, np)
        
        # Обновляем информацию о том, сколько раз встречалась данная позиция
        if self.position.notation() in self.positions_frequence:
            self.positions_frequence[self.position.notation()] += 1
        else:
            self.positions_frequence[self.position.notation()] = 1
        
        # Записываем ход и обновляем историю позиции
        self.positions_history.append(self.position)
        self.moves_history.append((y1, x1, y2, x2, np))
        
        # Определяем возможные ходы
        self.possible_moves = get_possible_moves(self)
        
        # Определимся, находится ли король под шахом, и если да, то на какой клетке
        self.in_check_kings_cell = get_in_check_kings_cell(self.position)
        
        # Запускаем событие on_move (ход сделан)
        self.dispatch('on_move', x1, y1, x2, y2, np)
        
        # Определяем статус и результат игры
        self.status, self.result = get_status_and_result(self)
        
        # Ход мы сделали, возвращаем True
        return True

