#-*-coding:utf-8-*-
#qpy:2
#qpy:kivy
#qpy:fullscreen

# Импорт нужных модулей
import os

from kivy.utils import platform
from kivy.app import App
from kivy.base import Builder
from kivy.core.window import Window

from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.modalview import ModalView

from kivy.graphics import Color, Rectangle

from game import Game
from enums import Sides, Pieces, CellColor, GameStatus, GameResult

# Константы
BASE_DIR = os.path.dirname(os.path.abspath(__file__))   # Папка, где находится наша программа
IMAGE_DIR = os.path.join(BASE_DIR, "res", "images")     # Папка с изображениями

ALTERNATIVE_WINDOW_SIZE = (450, 800)                    # Альтернативный размер окна для не андроид платформ (используется для теста на компьютере)


# Класс фигуры
class Piece(Image):

    # Инициализация класса фигуры
    def __init__(self, kind=None, **kwargs):
    
        # Определимся с тем, какой фигуре соответствует какой файл
        set_sources = {Pieces.WHITE_KING:     os.path.join(IMAGE_DIR, "white_king.png"),
                       Pieces.WHITE_QUEEN:    os.path.join(IMAGE_DIR, "white_queen.png"),
                       Pieces.WHITE_ROOK:     os.path.join(IMAGE_DIR, "white_rook.png"),
                       Pieces.WHITE_KNIGHT:   os.path.join(IMAGE_DIR, "white_knight.png"),
                       Pieces.WHITE_BISHOP:   os.path.join(IMAGE_DIR, "white_bishop.png"),
                       Pieces.WHITE_PAWN:     os.path.join(IMAGE_DIR, "white_pawn.png"),
                       Pieces.BLACK_KING:     os.path.join(IMAGE_DIR, "black_king.png"),
                       Pieces.BLACK_QUEEN:    os.path.join(IMAGE_DIR, "black_queen.png"),
                       Pieces.BLACK_ROOK:     os.path.join(IMAGE_DIR, "black_rook.png"),
                       Pieces.BLACK_KNIGHT:   os.path.join(IMAGE_DIR, "black_knight.png"),
                       Pieces.BLACK_BISHOP:   os.path.join(IMAGE_DIR, "black_bishop.png"),
                       Pieces.BLACK_PAWN:     os.path.join(IMAGE_DIR, "black_pawn.png")}
        
        # Вычисляем путь к файлу спрайта
        source = set_sources[kind]
        
        # Вызываем конструктор базового класса
        super(self.__class__, self).__init__(source=source, allow_stretch=True, keep_ratio=False, **kwargs)


# Класс клетки
class Cell(RelativeLayout):

    # Инициализация класса клетки
    def __init__(self, board, row, col, piece=None, **kwargs):
        
        # Запоминаем координаты и саму доску
        self.board = board
        self.coord = (row, col)
        
        # Определимся с тем, какому цвету соответствует какой файл
        set_sources = {CellColor.WHITE:   os.path.join(IMAGE_DIR, "white_cell.png"),
                       CellColor.BLACK:   os.path.join(IMAGE_DIR, "black_cell.png")}
        
        # Вычисляем цвет клетки
        cell_color = CellColor.BLACK if ((row + col) % 2 == 0) else CellColor.WHITE
        
        # Вычисляем путь к файлу спрайта
        if self.coord in board.possible_target_squares:
            source = os.path.join(IMAGE_DIR, "possible_target_square.png")
        elif (board.current_move_to_cell is not None) and (cmp(self.coord, board.current_move_to_cell) == 0):
            source = os.path.join(IMAGE_DIR, "current_move_to_cell.png")
        elif (board.current_move_from_cell is not None) and (cmp(self.coord, board.current_move_from_cell) == 0):
            source = os.path.join(IMAGE_DIR, "current_move_from_cell.png")
        elif (board.in_check_kings_cell is not None) and (cmp(self.coord, board.in_check_kings_cell) == 0):
            source = os.path.join(IMAGE_DIR, "in_check_kings_cell.png")
        elif (board.last_move_to_cell is not None) and (cmp(self.coord, board.last_move_to_cell) == 0):
            source = os.path.join(IMAGE_DIR, "last_move_to_cell.png")
        elif (board.last_move_from_cell is not None) and (cmp(self.coord, board.last_move_from_cell) == 0):
            source = os.path.join(IMAGE_DIR, "last_move_from_cell.png")
        else:
            source = set_sources[cell_color]
        
        # Отображаем эту клетку
        super(self.__class__, self).__init__(**kwargs)
        self.add_widget(Image(source=source, allow_stretch=True, keep_ratio=False))
        
        # Рисуем фигуру
        self.piece = None
        if piece is not None:
            self.piece = Piece(kind=piece)
            self.add_widget(self.piece)
            
    # Обработка нажатия
    def on_touch_down(self, touch):
        
        # Если нажали именно эту клетку
        if self.collide_point(*touch.pos):
            
            # Определимся можно ли сделать ход с этой клетки и возможные ходы с этой клетки
            # Также определимся можно ли сделать ход на эту клетку и если можно, то какой это ход: превращение пешки в фигуру или нет
            can_move_from_the_cell = False
            can_move_to_the_cell = False
            possible_target_squares = []
            is_it_promotion = False
            for move in self.board.game.possible_moves:
                if cmp((move[1], move[0]), self.coord) == 0:
                    can_move_from_the_cell = True
                    possible_target_squares.append((move[3], move[2]))
                if self.board.current_move_from_cell is not None:
                    if cmp((move[1], move[0]), self.board.current_move_from_cell) == 0:
                        if cmp((move[3], move[2]), self.coord) == 0:
                            can_move_to_the_cell = True
                            if move[4] is not None:
                                is_it_promotion = True
                            
            # Если есть выделенная фигура
            if self.board.piece_selected:
                
                # Если можно сделать ход на эту клетку, то делаем ход
                if can_move_to_the_cell:
                
                    # Если пешка превращается в фигуру, то показываем всплывающее окно, в котором выбираем новую фигуру, а потом делаем ход
                    if is_it_promotion:
                        popup = PromotionModalView(self.board, self.coord)
                        popup.open()
                        
                    # Иначе сразу совершаем ход
                    else:
                        self.board.game.do_move(self.board.current_move_from_cell[1], self.board.current_move_from_cell[0], self.coord[1], self.coord[0], None)
                    
                # Если нельзя сделать ход на эту клетку, но с этой можно, и эта не та же клетка, что и до этого, то перевыбираем активную фигуру
                elif can_move_from_the_cell and (not((self.board.current_move_from_cell is not None) and (cmp(self.board.current_move_from_cell, self.coord) == 0))):
                    self.board.current_move_from_cell = self.coord
                    self.board.current_move_to_cell = None
                    self.board.possible_target_squares = possible_target_squares
                    self.board.update()
                    
                # Если нельзя сделать ход вообще, то отменяем все: убирается выделение с активной фигуры
                else:
                    self.board.current_move_from_cell = None
                    self.board.current_move_to_cell = None
                    self.board.possible_target_squares = []
                    self.board.update()
                    
            # Если нет выделенной фигуры
            else:
                
                # Если можно сделать ход с этой клетки, то выделяется данная клетка и показывается предыдущий ход
                if can_move_from_the_cell:
                    if self.board.current_move_from_cell is not None:
                        self.board.last_move_from_cell = self.board.current_move_from_cell
                    if self.board.current_move_to_cell is not None:
                        self.board.last_move_to_cell = self.board.current_move_to_cell
                    self.board.current_move_from_cell = self.coord
                    self.board.current_move_to_cell = None
                    self.board.piece_selected = True
                    self.board.possible_target_squares = possible_target_squares
                    self.board.update()                  


# Класс макета вариантов превращения пешки в фигуру
class PromotionLayout(RelativeLayout):
    
    # Инициализация макета вариантов превращения пешки в фигуру
    def __init__(self, popup, piece_kind, **kwargs):
    
        # Запоминаем ссылку на всплывающее окно и тип фигуры
        self.popup = popup
        self.piece_kind = piece_kind
        
        # Показываем соответствующую фигуру
        super(self.__class__, self).__init__(**kwargs)
        source = os.path.join(IMAGE_DIR, "promotion_layout_background.png")
        self.add_widget(Image(source=source, allow_stretch=True, keep_ratio=False))
        self.add_widget(Piece(kind=piece_kind))
        
    # Обработка нажатия
    def on_touch_down(self, touch):
        
        # Если выбрали именно эту фигуру
        if self.collide_point(*touch.pos):
            
            # Закрываем всплывающее окно и делаем ход
            self.popup.dismiss()
            self.popup.board.game.do_move(self.popup.board.current_move_from_cell[1], self.popup.board.current_move_from_cell[0], self.popup.coord[1], self.popup.coord[0], self.piece_kind)


# Всплывающее окно выбора новой фигуры
class PromotionModalView(ModalView):
    # TODO: надо реализовать
    
    # Инициализация всплывающего виджета
    def __init__(self, board, coord, **kwargs):
        
        # Запоминаем доску и клетку, на которую хотим поставить новую фигуру
        self.board = board
        self.coord = coord
        
        # Определимся с тем, какой фигуре соответствует какой файл
        set_sources = {Pieces.WHITE_QUEEN:    os.path.join(IMAGE_DIR, "white_queen.png"),
                       Pieces.WHITE_ROOK:     os.path.join(IMAGE_DIR, "white_rook.png"),
                       Pieces.WHITE_KNIGHT:   os.path.join(IMAGE_DIR, "white_knight.png"),
                       Pieces.WHITE_BISHOP:   os.path.join(IMAGE_DIR, "white_bishop.png"),
                       Pieces.BLACK_QUEEN:    os.path.join(IMAGE_DIR, "black_queen.png"),
                       Pieces.BLACK_ROOK:     os.path.join(IMAGE_DIR, "black_rook.png"),
                       Pieces.BLACK_KNIGHT:   os.path.join(IMAGE_DIR, "black_knight.png"),
                       Pieces.BLACK_BISHOP:   os.path.join(IMAGE_DIR, "black_bishop.png")}
        
        # Определимся какие фигуры отражать и какие у них координаты
        possible_new_white_pieces = (Pieces.WHITE_QUEEN, Pieces.WHITE_ROOK, Pieces.WHITE_KNIGHT, Pieces.WHITE_BISHOP)
        possible_new_black_pieces = (Pieces.BLACK_QUEEN, Pieces.BLACK_ROOK, Pieces.BLACK_KNIGHT, Pieces.BLACK_BISHOP)
        possible_new_pieces = possible_new_white_pieces if self.board.game.position.whose_turn == Sides.WHITE else possible_new_black_pieces
        
        # Отображаем всплывающее окно
        super(self.__class__, self).__init__(auto_dismiss=False, size_hint=(None, None), size=(self.board.width * 0.3, self.board.width * 0.3), **kwargs)
        self.grid_layout = GridLayout(size_hint=(None, None), size=(self.board.width * 0.3, self.board.width * 0.3), rows=2, cols=2)
        self.add_widget(self.grid_layout)
        
        # Отображаем варианты превращения
        for piece_kind in possible_new_pieces:
            self.grid_layout.add_widget(PromotionLayout(self, piece_kind))


# Всплывающее окно о завершении игры
class GameOverModalView(ModalView):
    
    # Инициализация окна о завершении игры
    def __init__(self, board, message="", **kwargs):
        
        # Показываем сообщение о завершении игры
        super(self.__class__, self).__init__(size_hint=(None, None), size=(board.width * 0.8, board.width * 0.2), background_color=(0, 0, 0, 0), **kwargs)
        self.add_widget(Label(text=message))


# Класс доски
class Board(GridLayout):
    
    # Инициализация класса доски
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        self.parent = None
        
        # Доска у нас чистая: ни одна клетка не выделена
        self.game = None
        self.last_move_from_cell = None
        self.last_move_to_cell = None
        self.current_move_from_cell = None
        self.current_move_to_cell = None
        self.piece_selected = False
        self.possible_target_squares = []
        self.in_check_kings_cell = None
        
    # Перерисовывает доску в зависимости от позиции в игре
    def update(self):
        if self.game is not None:
            self.clear_widgets()
            for row in range(7, -1, -1):
                for col in range(8):
                    cell = Cell(self, row, col, piece=self.game.position.piece_placement[row][col])
                    self.add_widget(cell)
                    
    # Обработчик события on_start_new (началась новая игра)
    def on_start_new(self, game):
        self.update()
    
    # Обработчик события on_move (ход сделан)
    def on_move(self, game, x1, y1, x2, y2, np):
    
        # Обновляем доску
        self.current_move_to_cell = (y2, x2)
        self.last_move_from_cell = None
        self.last_move_to_cell = None
        self.piece_selected = False
        self.possible_target_squares = []
        self.in_check_kings_cell = game.in_check_kings_cell
        self.update()
        
    # Обработчик на изменение свойства статус игры
    def on_status_change(self, game, status):
        
        # Определимся с тем, какое сообщение выводить в зависимости от статуса игры
        set_messages = {GameStatus.WHITE_HAS_BEEN_CHECKMATED:   'Черные поставили мат белым!',
                        GameStatus.BLACK_HAS_BEEN_CHECKMATED:   'Белые поставили мат черным!',
                        GameStatus.STALEMATE:                   'Пат!'}
                        
        # Если для статуса определено сообщение, то выводим это сообщение
        if status in set_messages:
            popup = GameOverModalView(self, set_messages[status])
            popup.open()


# Экран игры
class GameScreen(Screen):

    # Инициализация экрана игры
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        
        # Принудительно изменяем размер окна, если запускается не с телефона
        if platform != 'android':
            Window.size = ALTERNATIVE_WINDOW_SIZE
            self.size = Window.size
            
        # Создаем игру и привязываем к нему обработчиков событии
        self.board = self.ids.board
        self.board.game = Game()
        self.board.game.bind(on_start_new=self.board.on_start_new)
        self.board.game.bind(on_move=self.board.on_move)
        self.board.game.bind(status=self.board.on_status_change)
        self.board.game.start_new()


# Вся разметка
Builder.load_string("""
<GameScreen>:
    Board:
        id: board
        size_hint: None, None
        size: root.width, root.width
        pos: 0, (root.height-root.width)/2
        rows: 8
        cols: 8
        spacing: 6
        padding: 6
        canvas.before:
            Color:
                rgba: .3, .3, .3, 1
            Rectangle:
                pos: self.pos
                size: self.size
""")


# Наш менеджер экранов
class MyScreenManager(ScreenManager):

    # Инициализация нашего менеджера экранов
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        self.transition = FadeTransition()
        self.add_widget(GameScreen(name='game'))


# Наше основное приложение
class ChessApp(App):
    def build(self):
        return MyScreenManager()


# Выполнение программы начинается здесь
if __name__ == '__main__':
    ChessApp().run()

