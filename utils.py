# -*- coding: utf-8 -*-

# Импорт нужных модулей
from enums import Pieces, Sides
from enums import WHITE_PIECES, BLACK_PIECES, NOT_PAWN_PIECES


# Возвращает True, если данная фигура является королем
def is_king(p):
    return p in [Pieces.WHITE_KING, Pieces.BLACK_KING]


# Возвращает True, если данная фигура является ферзем
def is_queen(p):
    return p in [Pieces.WHITE_QUEEN, Pieces.BLACK_QUEEN]


# Возвращает True, если данная фигура является ладьей
def is_rook(p):
    return p in [Pieces.WHITE_ROOK, Pieces.BLACK_ROOK]


# Возвращает True, если данная фигура является конем
def is_knight(p):
    return p in [Pieces.WHITE_KNIGHT, Pieces.BLACK_KNIGHT]


# Возвращает True, если данная фигура является слоном
def is_bishop(p):
    return p in [Pieces.WHITE_BISHOP, Pieces.BLACK_BISHOP]


# Возвращает True, если данная фигура является пешкой
def is_pawn(p):
    return p in [Pieces.WHITE_PAWN, Pieces.BLACK_PAWN]


# Возвращает True, если данная фигура не является пешкой
def is_not_pawn(p):
    return p in NOT_PAWN_PIECES


# Определяет цвет фигуры
def piece_color(p):
    if p in WHITE_PIECES:
        return Sides.WHITE
    elif p in BLACK_PIECES:
        return Sides.BLACK
    return None


# Возвращает знак числа
def sign(number):
    if number > 0:
        return 1
    elif number < 0:
        return -1
    else:
        return 0

